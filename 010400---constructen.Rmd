# Constructen {#constructen}

Psychologen bestuderen mensen, of beter gezegd, wat mensen doen, waarnemen, voelen, en denken. Wat mensen doen wordt bepaald door hun hersenen, en is het gevolg van onder andere waarneming, gevoel, en gedachten.

## Neuronen en constructen

Die hersenen bestaan uit ongeveer 90 miljard neuronen die elk met gemiddeld 7000 andere neuronen zijn verbonden (dus dat zijn ongeveer 630 000 000 000 000, oftwel 630 000 miljard, verbindingen). Er bestaat geen manier om rechtstreeks te observeren wat al die neuronen precies doen. Bovendien kunnen neuronen ongeveer 200 keer per seconde "vuren" (andere neuronen activeren of inhiberen), dus ongeveer 720 000 keer per uur. Zelfs als het mogelijk zou zijn om alle neuronen te observeren, dan zou het opslaan van de benodigde data heel snel uit de hand lopen: een onderzoek van een uur zou al de opslag van 453 600 000 000 000 000 000 datapunten vereisen.^[Als het opslaan van zo'n datapunt slechts een byte zou kosten, dan zou de opslag van deze datapunten ongeveer 453 exabytes vereisen, oftewel 453 miljard gigabytes - hoewel dit natuurlijk gecomprimeerd kan worden opgeslagen.]

Gedachten, gevoelens, waarnemingen, en herinneringen bestaan in onze psychologie dus uit miljoenen of miljarden hersencellen en verbindingen, waarbij voorlopig niet in kaart gebracht kan worden hoe die precies zijn gerepresenteerd. Zaken als persoonlijkheid, hoe mensen beslissingen nemen, hoe mensen stress ervaren en met stress omgaan, en waarom sommige mensen liever een glas wijn drinken en anderen liever ecstasy gebruiken zijn dus niet rechtstreeks te bestuderen.

Bij het bestuderen van mensen worden daarom vaak zogenaamde *constructen* bestudeerd. Maar wat is zo'n construct? Hier zijn allerlei opvattingen over. We bespreken er hier vier: natuurlijk soorten, sociale soorten, praktische soorten, en complexe soorten [@fried_what_2017].

## Soorten: vier perspectieven op constructen

In andere wetenschappen worden vaak natuurlijke soorten bestudeerd. Natuurlijke soorten bestaan los van de mensheid, los van namen en definities. Atomen en moleculen zijn hier voorbeelden van: de eigenschappen van water, $H_2O$, veranderen niet als je het anders noemt, en alle moleculen met twee waterstof-atomen en een zuurstofatoom zijn water. Water is een natuurlijke soort.

Een andere soort is de sociale soort. Veel dingen bestaan niet los van wat mensen er van vinden en er mee doen, maar worden juist uitgevonden door mensen als middel om de wereld te bestuderen. Persoonlijkheid is hier een voorbeeld van: constructen zoals extraversie of openheid bestaan niet op dezelfde manier als water bestaat. Het feit dat er meerdere definities van persoonlijkheid zijn, die soms vijf, soms zes, of soms nog een ander aantal persoonlijkheidstrekken omvatten, laat dit bijvoorbeeld zien. Tegelijkertijd geldt voor bijvoorbeeld emoties zoals blijdschap, of fenomenen zoals het geheugen, dat ze zich niets aantrekken van hoe mensen het noemen.

Een oplossing is om psychologische constructen als praktische soorten te zien. Dan is het niet zo belangrijk meer of ze wel echt als zodanig bestaan: hun waarde wordt dan ontleend aan hoe nuttig ze zijn. We kunnen bijvoorbeeld een contruct bedenken dat "Methostatie" heet, en we kunnen het definieren als het gemiddelde van iemands scores op drie curssusen, zoals de onderzoekspractica Inleiding Data-Analyse, Kwalitatief Onderzoek, en Cross-sectioneel Onderzoek. Het moge duidelijk zijn dat methostatie niet als zodanig bestaat in het hoofd van mensen. Tegelijkertijd kan het heel nuttig zijn om iemands methostatie te berekenen als je hun cijfer op de bachelor- of masterthese wil voorspellen.

Tot slot zijn er de complexe soorten. Als je constructen ziet als een complexe soort zie je ze als een verzameling eigenschappen die vaak samen voorkomen omdat ze elkaar wederzijds beïnvloedden. Als iemand bijvoorbeeld van de smaak van koffie houdt, zal diegene waarschijnlijk (maar niet noodzakelijk) ook van de geur van koffie houden; waarschijnlijk (maar niet noodzakelijk) koffie drinken gezellig vinden; en waarschijnlijk (maar niet noodzakelijk) regelmatig koffie drinken. Deze combinatie van eigenschappen zou je bijvoorbeeld een "koffieliefhebber" kunnen noemen.

De eigenschappen hoeven niet altijd precies hetzelfde te zijn, zoals bijvoorbeeld bij water (*altijd* $H_2O$). Als je een stoel als een complexe soort ziet, dan zou die kunnen zijn gedefinieerd als bestaande uit horizontaal een vlak van ongeveer 45 bij 45 centimeter, op ongeveer 45 centimeter hoogte, gedragen door vier poten, met een verticaal vlak van ongeveer 50 centimeter hoog aan een zijde van het horizontale vlak geplaatst. Het is waar dat als bijvoorbeeld twee van deze eigenschappen samen voorkomen (een horizontaal vlak van ongeveer 45 bij 45 centimeter op vier poten) de kans groot is dat die vier poten ongeveer 45 centimeter hoog zijn, en dat het geheel vergezeld gaat van een achterleuning van ongeveer 50 centimeter. Tegelijkertijd zijn er ook stoelen met drie poten, of vijf; grotere en kleinere stoelen; en stoelen zonder achterleuning (hoewel je die ook een krukje zou kunnen noemen).

Psychologische constructen worden tegenwoordig steeds vaker onderzocht als complexe soorten, waarbij ze worden gezien als netwerk van eigenschappen. Mensen die extravert zijn, vinden vaak sociaal gezelschap prettig, vinden sociale situaties vaak niet stressvol, praten vaak veel veel, staan vaak op de voorgrond, en starten vaak gesprekken, om een paar eigenschappen te noemen. En andersom, als mensen sociale situaties stressvol vinden en niet graag op de voorgrond staan, is de kans dat ze veel praten, sociaal gezelschap prettig vinden en vaak op de voorgrond staan al kleiner. Zo bezien beschrijft "extraversie" een set van eigenschappen die vaak samen voorkomt.

## Wat zijn psychologische constructen?

Het is mogelijk dat al deze perspectieven op psychologische constructen tegelijkertijd bestaan omdat die constructen niet rechtstreeks observeerbaar zijn. Er is geen antwoord op de vraag wat psychologische constructen zijn: of het antwoord moet "we weten het niet" zijn. Het is onwaarschijnlijk dat constructen als een soort discrete, modulaire bouwsteentjes onze psychologie vormen [@peters_pragmatic_2017]. Psychologische constructen zijn niet vergelijkbaar met bijvoorbeeld temperatuur, luchtdruk, of geluid; of met atomen of moleculen; of met cellen of organen; of met soorten planten of onderdelen van een machine; of met onderdelen van een computer of een computerprogramma [@peters_confidence_2017]. Elk van die analogiën kan nuttig zijn bij het nadenken over psychologische constructen; maar het is belangrijk te onthouden dat dit metaforen zijn, en dus geen beschrijvingen van de realiteit.

Er is geen antwoord op wat constructen in realiteit dan wel zijn. Er is geen overduidelijk correct ontologisch standpunt^[Zie hoofdstuk [Wetenschap](https://openmens.nl/wetenschap).] dat we kunnen innemen met betrekking tot psychologische constructen. Dit maakt onderzoek naar mensen natuurlijk niet eenvoudiger. We bestuderen dingen die we niet rechtstreeks kunnen observeren, en die niet eens echt als zodanig bestaan (i.e. "extraversie" bestaat niet op dezelfde manier als een stoel bestaat). Toch willen we die meten en proberen we om ze te gebruiken om mensen beter te begrijpen en voorspellen.

Omdat psychologische constructen niet rechtstreeks observeerbaar zijn, en omdat er allerlei ontologische posities te verdedigen zijn, en omdat mensen zo goed zijn in patroonherkenning dat ze regelmatig patronen waarnemen waar die niet bestaan, en omdat mensen zelf de ervaring hebben prima na te kunnen denken over hoe zijzelf werken en hoe anderen werken, is het in de psychologie extra belangrijk om de wetenschappelijke methode rigoreus toe te passen. In dit boek wordt de basis daarvoor gelegd.

Het ontologische perspectief dat je hanteert met betrekking tot een psychologisch construct en de definitie van dat construct bepalen samen hoe je na moet denken over het manipuleren en/of meten van dat construct. Dit komt verder aan bod in hoofdstuk [Constructen Meten](https://openmens.nl/constructen-meten).

## Een metafoor om over constructen na te denken

Een handige metafoor om over constructen te denken is een netwerk van psychologische aspecten. Een ijsje heeft bijvoorbeeld een psychologische representatie; mensen kunnen gesprekken voeren over ijsjes, ze kunnen er herinneringen over ophalen, en hebben verschillende voorkeuren met betrekking tot ijsjes in het algemeen of specifieke ijsjes in het bijzonder. Figuur \@ref(fig:constructen-pn-metafoor) verschaft een metafoor om een stukje van de psychologie te visualiseren, specifiek met betrekking tot of iemand van plan is om een ijsje te gaan eten [zie voor meer achtergrond over deze metafoor @peters_pragmatic_2017].

```{dot constructen-pn-metafoor, fig.cap="Een metafoor voor constructen.", fig.width=1800, fig.ext="png", echo=FALSE}

### Running this chunk requires GraphViz to be locally installed!
### See https://rdrr.io/cran/ndtv/man/install.graphviz.html
###
### This can conveniently be tested online in realtime at
### https://dreampuf.github.io/GraphvizOnline


graph {

  graph [layout="neato", rankdir="LR", overlap="scale", pad=".5", ranksep="1", nodesep="1"];

  node [shape=circle, penwidth=2, width=1, fontname="arial"]; 
  
  edge [penwidth=2];

  shame           [label="Schaamte"]
  guiltiness      [label="Schuld\ngevoel"]
  pleasantness    [label="Genot"]
  interestingness [label="Interessant-\nheid"]
  obstacles       [label="Aanwezige\nobstakels"]
  skill           [label="Vaardig\nheid"]
  easiness        [label="Gemak"]
  etiquette       [label="Volgens de\netiquette"]
  scariness       [label="Angst-\naanjagend-\nheid"]
  partner_appr    [label="Goed-\nkeuring\npartner"]
  partner_behav   [label="Gedrag\nvan\npartner"]
  parents_appr    [label="Goed-\nkeuring\nouders"]
  cleanness       [label="Netheid"]
  harmfulness     [label="Schade-\nlijk\nheid"]
  beneficiality   [label="Voor-\ndeligheid"]
  goodness        [label="Goedheid"]
  satisfaction    [label="Bevredigend-\nheid"]
  fun             [label="Lol"]
  excitingness    [label="Spannend-\nheid"]
  
  shame           -- guiltiness       [penwidth=2];
  shame           -- pleasantness     [penwidth=2];
  shame           -- beneficiality    [penwidth=2];
  
  pleasantness    -- guiltiness       [penwidth=2];
  pleasantness    -- satisfaction     [penwidth=2];
  pleasantness    -- goodness         [penwidth=2];
  pleasantness    -- harmfulness      [penwidth=2];
  pleasantness    -- interestingness  [penwidth=2];
  
  interestingness -- harmfulness      [penwidth=2, weight=20, len=20];
  interestingness -- easiness         [penwidth=2];
  interestingness -- obstacles        [penwidth=2];
  interestingness -- cleanness        [penwidth=2];
  interestingness -- fun              [penwidth=2];
  interestingness -- goodness         [penwidth=2];
  
  obstacles       -- skill            [penwidth=2];
  obstacles       -- easiness         [penwidth=2];
  obstacles       -- fun              [penwidth=2];
  obstacles       -- goodness         [penwidth=2];
  
  skill           -- excitingness     [penwidth=2];
  skill           -- easiness         [penwidth=2];
  skill           -- etiquette        [penwidth=2];

  etiquette       -- excitingness     [penwidth=2];
  etiquette       -- parents_appr     [penwidth=2];
  etiquette       -- cleanness        [penwidth=2];
  etiquette       -- scariness        [penwidth=2];
  etiquette       -- easiness         [penwidth=2];
  
  scariness       -- excitingness     [penwidth=2];
  scariness       -- partner_appr     [penwidth=2];
  scariness       -- partner_behav    [penwidth=2];
  scariness       -- parents_appr     [penwidth=2];

  cleanness       -- parents_appr     [penwidth=2];
  cleanness       -- easiness         [penwidth=2];
  cleanness       -- harmfulness      [penwidth=2];
  cleanness       -- beneficiality    [penwidth=2];

  goodness        -- beneficiality    [penwidth=2];
  goodness        -- fun              [penwidth=2];
  goodness        -- satisfaction     [penwidth=2];
  goodness        -- harmfulness      [penwidth=2];
  
  partner_behav   -- partner_appr     [penwidth=2];
  partner_behav   -- parents_appr     [penwidth=2];
  
  fun             -- satisfaction     [penwidth=2];
  
  guiltiness      -- satisfaction     [penwidth=2];
  
  harmfulness     -- easiness         [penwidth=2];
  harmfulness     -- beneficiality    [penwidth=2, len=4];

}

```

Deze persoon ziet "een ijsje eten" in een bepaalde mate als iets goeds; in een bepaalde mate als iets leuks; beschouwt het in een bepaalde mate als iets makkelijks; en heeft een bepaalde associatie met schuldgevoel. Deze metafoor heeft, net als elke metafoor die je kunt inzetten als middel om grip te proberen te krijgen om de menselijke psychologie, een aantal problemen.

Ten eerste zijn de hier afgebeelde aspecten in zekere zin arbitrair; er zijn er duizenden te bedenken. En niet elke aspect is even relevant voor alle situaties: hoe gezond iets is speelt wellicht een grotere rol bij het besluit om al dan niet te gaan sporten dan bij het besluit welke film iemand wil gaan kijken.

Ten tweede is de "specificiteit" van deze mogelijke aspecten in zekere zin arbitrair; ze zijn hier gekozen omdat ze eenvoudig uit te drukken zijn in taal, maar dat verleent de aspecten nog geen speciale rol of 'waarheid', het vergemakkelijkt alleen de communicatie. Hoe "gezond" iemand iets vindt kan net zo goed worden opgeplists in "fysieke gezondheid" en "geestelijke gezondheid", en ook die opdeling is arbitrair.

Ten derde werkt deze metafoor veel beter om over sommige situaties na te denken dan anderen. Bij het nadenken over menselijk gedrag kan deze metafoor erg nuttig zijn; maar bij het nadenken over aandacht of geheugen zijn dergelijke representaties minder nuttig.

Desondanks gaat psychologie vaak over het verklaren of veranderen van menselijk gedrag, en vaak kunnen zinvolle aspecten worden onderscheiden, waardoor deze metafoor vaak wel bruikbaar kan zijn bij het nadenken over constructen. Na deze disclaimer, dus terug naar ons voorbeeld van het ijsje.

We zouden nu een psychologisch construct kunnen definiëren in termen van de aspecten van de menselijke psychologie die dat construct wel en juist niet zou moeten beschrijven. We zouden bijvoorbeeld kunnen stellen dat iemands "attitude ten opzichte van het eten van een ijsje" bestaat uit de mate waarin die persoon het eten van een ijsje evalueert als goed, onschadelijk, en voordelig. Een ander construct, "waargenomen normen met betrekking tot het eten van een ijsje" kunnen we vervolgens definieren als de mate waarin iemand denkt dat hun partner en ouders het zouden goedkeuren als diegene een ijsje zou eten en wat de partner van diegene doet. Dit kunnen we vervolgens zo visualiseren zoals weergegeven in Figuur \@ref(fig:constructen-pn-metafoor-met-twee-constructen).

```{dot constructen-pn-metafoor-met-twee-constructen, fig.cap="Attitude en Waargenomen Normen geïllustreerd.", fig.width=1800, fig.ext="png", echo=FALSE}

### Running this chunk requires GraphViz to be locally installed!
### See https://rdrr.io/cran/ndtv/man/install.graphviz.html
###
### This can conveniently be tested online in realtime at
### https://dreampuf.github.io/GraphvizOnline

graph {

  graph [layout="neato", rankdir="LR", overlap="scale", pad=".5", ranksep="1", nodesep="1"];

  node [shape=circle, penwidth=2, width=1, fontname="arial"]; 
  
  edge [penwidth=2];

  shame           [label="Schaamte"]
  guiltiness      [label="Schuld\ngevoel"]
  pleasantness    [label="Genot"]
  interestingness [label="Interessant-\nheid"]
  obstacles       [label="Aanwezige\nobstakels"]
  skill           [label="Vaardig\nheid"]
  easiness        [label="Gemak"]
  etiquette       [label="Volgens de\netiquette"]
  scariness       [label="Angst-\naanjagend-\nheid"]
  partner_appr    [label="Goed-\nkeuring\npartner", fontcolor="black", style="filled", fillcolor="grey"]
  partner_behav   [label="Gedrag\nvan\npartner", fontcolor="black", style="filled", fillcolor="grey"]
  parents_appr    [label="Goed-\nkeuring\nouders", fontcolor="black", style="filled", fillcolor="grey"]
  cleanness       [label="Netheid"]
  harmfulness     [label="Schade-\nlijk\nheid", fontcolor="white", style="filled", fillcolor="black"]
  beneficiality   [label="Voor-\ndeligheid", fontcolor="white", style="filled", fillcolor="black"]
  goodness        [label="Goedheid", fontcolor="white", style="filled", fillcolor="black"]
  satisfaction    [label="Bevredigend-\nheid"]
  fun             [label="Lol"]
  excitingness    [label="Spannend-\nheid"]
  
  shame           -- guiltiness       [penwidth=2];
  shame           -- pleasantness     [penwidth=2];
  shame           -- beneficiality    [penwidth=2];
  
  pleasantness    -- guiltiness       [penwidth=2];
  pleasantness    -- satisfaction     [penwidth=2];
  pleasantness    -- goodness         [penwidth=2];
  pleasantness    -- harmfulness      [penwidth=2];
  pleasantness    -- interestingness  [penwidth=2];
  
  interestingness -- harmfulness      [penwidth=2, weight=20, len=20];
  interestingness -- easiness         [penwidth=2];
  interestingness -- obstacles        [penwidth=2];
  interestingness -- cleanness        [penwidth=2];
  interestingness -- fun              [penwidth=2];
  interestingness -- goodness         [penwidth=2];
  
  obstacles       -- skill            [penwidth=2];
  obstacles       -- easiness         [penwidth=2];
  obstacles       -- fun              [penwidth=2];
  obstacles       -- goodness         [penwidth=2];
  
  skill           -- excitingness     [penwidth=2];
  skill           -- easiness         [penwidth=2];
  skill           -- etiquette        [penwidth=2];

  etiquette       -- excitingness     [penwidth=2];
  etiquette       -- parents_appr     [penwidth=2];
  etiquette       -- cleanness        [penwidth=2];
  etiquette       -- scariness        [penwidth=2];
  etiquette       -- easiness         [penwidth=2];
  
  scariness       -- excitingness     [penwidth=2];
  scariness       -- partner_appr     [penwidth=2];
  scariness       -- partner_behav    [penwidth=2];
  scariness       -- parents_appr     [penwidth=2];

  cleanness       -- parents_appr     [penwidth=2];
  cleanness       -- easiness         [penwidth=2];
  cleanness       -- harmfulness      [penwidth=2];
  cleanness       -- beneficiality    [penwidth=2];

  goodness        -- beneficiality    [penwidth=2];
  goodness        -- fun              [penwidth=2];
  goodness        -- satisfaction     [penwidth=2];
  goodness        -- harmfulness      [penwidth=2];
  
  partner_behav   -- partner_appr     [penwidth=2];
  partner_behav   -- parents_appr     [penwidth=2];
  
  fun             -- satisfaction     [penwidth=2];
  
  guiltiness      -- satisfaction     [penwidth=2];
  
  harmfulness     -- easiness         [penwidth=2];
  harmfulness     -- beneficiality    [penwidth=2, len=4];

}

```

Deze visualisatie maakt het makkelijker om grondig na te denken over definities van constructen. Zou bijvoorbeeld de mate waarin een ijsje eten volgens de etiquette is, niet ook deel uit moeten maken van de norm? Of zou de mate waarin een ijsje eten interessant is niet ook deel uit moeten maken van attitude?

Omdat constructen niet rechtstreeks observeerbaar zijn, moeten ze zodanig expliciet gedefinieerd zijn dat de definitie helderheid geeft over de inhoud van het construct. Die definitie geeft







