tryCatch({
  bibCon <- url("https://api.zotero.org/groups/2387508/items?format=bibtex");
  bibliography<-readLines(bibCon, warn=FALSE);
  close(bibCon);
}, error=function(e) {
  cat("Error downloading the references from Zotero's API:", e$message);
});

writeLines(bibliography, here::here("openmens.bib"));

cat(bibliography,
    sep="\n");

print(gsub("^@[a-zA-Z0-9]+\\{(.*),",
           "\\1",
           bibliography)[grep("^@[a-zA-Z0-9]+\\{(.*),", bibliography)])
